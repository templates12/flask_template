#!/usr/bin/env bash
set -e

docker-compose stop
docker-compose build
docker-compose run rest bash -c 'bash -s <<EOF
export FLASK_APP=project.restapp:app
flask_manage drop_db
flask_manage db upgrade
flask_admin roles create admin
flask_admin users create --password 123456 -a admin@foo.bar
flask_admin roles add admin@foo.bar admin
EOF'

docker-compose up -d
