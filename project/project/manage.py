import os
import shutil

import flask_script
import flask_migrate

from project import config
from project.instances import app
from project.instances import db

manager = flask_script.Manager(app)

# migrations
manager.add_command('db', flask_migrate.MigrateCommand)


@manager.command
def drop_db():
    """Drops the db tables."""
    db.reflect()
    db.drop_all()
    if os.path.exists(config.SERVER_FILES_DIR):
        shutil.rmtree(config.SERVER_FILES_DIR)


@manager.command
def add_test_data():
    """Creates test data."""


def main():
    manager.run()


if __name__ == '__main__':  # pragma: no cover
    main()
