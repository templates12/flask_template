import flask
import flask_httpauth
import flask_migrate
import flask_restful
import flask_sqlalchemy

app = flask.Flask(__name__)
app.config.from_pyfile('config.py')

basic_auth = flask_httpauth.HTTPBasicAuth()
token_auth = flask_httpauth.HTTPTokenAuth()
db = flask_sqlalchemy.SQLAlchemy(app)
migrate = flask_migrate.Migrate(app, db, directory='project/migrations')
api = flask_restful.Api(app)
