import datetime as dt

from depot import manager
from flask_restful import reqparse
from sqlalchemy.ext import declarative
import flask
import flask_restful
import flask_security
import itsdangerous

from project import config
from project.instances import db


manager.DepotManager.configure('default', {
    'depot.storage_path': config.SERVER_FILES_DIR
})

jwt = itsdangerous.TimedJSONWebSignatureSerializer(
    config.SECRET_KEY, expires_in=config.TOKEN_EXPIRES)


def get_current_user():
    return getattr(flask.g, 'user', None)


def get_current_user_id():
    user = get_current_user()
    if user:
        return user.id


class ModelSerializerMixin(object):
    __table_args__ = {'extend_existing': True}

    READONLY_FIELDS = ()
    HIDDEN_FIELDS = ()
    FIELD_TYPES = {}

    @classmethod
    def from_dict(cls, d):
        obj = cls()
        for key, value in d.items():
            if key in cls.READONLY_FIELDS:
                flask_restful.abort(
                    400, message='{!r} is read-only'.format(key))
            setattr(obj, key, value)
        return obj

    def update_from_dict(self, d):
        for key, value in d.items():
            if key in self.READONLY_FIELDS:
                flask_restful.abort(
                    400, message='{!r} is read-only'.format(key))
            setattr(self, key, value)

    def to_dict(self):
        d = {}
        for column in self.__table__.columns:
            if column.name in self.HIDDEN_FIELDS:
                continue
            attr = getattr(self, column.name)
            if isinstance(attr, dt.datetime):
                attr = attr.isoformat()
            d[column.name] = attr
        return d

    @classmethod
    def get_fields_parser(cls):
        parser = reqparse.RequestParser()
        for column in cls.__table__.columns:
            col_name = column.name
            if col_name in cls.HIDDEN_FIELDS:
                continue
            if col_name in cls.FIELD_TYPES:
                col_type = cls.FIELD_TYPES[col_name]
            else:
                try:
                    col_type = getattr(column.type, 'python_type')
                except NotImplementedError:
                    continue
            if col_type:
                parser.add_argument(col_name, type=col_type,
                                    store_missing=False, location='json')
                if col_type is not list:
                    parser.add_argument(col_name, type=col_type,
                                        store_missing=False, location='args')
        return parser


class CRUDMixin(object):
    __table_args__ = {'extend_existing': True}

    @classmethod
    def create(cls, **kwargs):
        instance = cls(**kwargs)
        return instance.save()

    @classmethod
    def get_or_create(cls, **kwargs):
        instance = cls.query.filter_by(**kwargs).one_or_none()
        if not instance:
            instance = cls.create(**kwargs)
        return instance

    def update(self, commit=True, **kwargs):
        for attr, value in kwargs.iteritems():
            setattr(self, attr, value)
        return commit and self.save() or self

    def save(self, commit=True):
        db.session.add(self)
        if commit:
            db.session.commit()
        return self

    def delete(self, commit=True):
        db.session.delete(self)
        return commit and db.session.commit()

    def refresh(self):
        db.session.refresh(self)


class BaseModel(db.Model, CRUDMixin):
    __abstract__ = True

    READONLY_FIELDS = ('id', 'created_on', 'updated_on')

    id = db.Column(db.Integer, primary_key=True)
    created_on = db.Column(db.DateTime, default=dt.datetime.utcnow)
    updated_on = db.Column(db.DateTime, default=dt.datetime.utcnow,
                           onupdate=dt.datetime.utcnow)

    @declarative.declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()


roles_users = db.Table(
    'roles_users',
    db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
    db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))


class User(BaseModel, flask_security.UserMixin):
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))

    def __str__(self):
        return self.email

    def verify_password(self, password):
        return flask_security.utils.verify_password(password, self.password)

    def generate_auth_token(self):
        return jwt.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        try:
            data = jwt.loads(token)
        except itsdangerous.SignatureExpired:
            return None    # valid token, but expired
        except itsdangerous.BadSignature:
            return None    # invalid token
        user = User.query.get(data['id'])
        return user


class Role(BaseModel, flask_security.RoleMixin):
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

    def __str__(self):
        return self.name
