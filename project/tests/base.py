import json

from depot import manager
from flask import testing
import flask_testing

from project import models
from project import resources  # noqa
from project.instances import app
from project.instances import db


manager.DepotManager.configure('test', {
    'depot.backend': 'depot.io.memory.MemoryFileStorage',
})
manager.DepotManager.set_default('test')


class CustomClient(testing.FlaskClient):
    """Allows to set auth header"""

    def __init__(self, *args, **kwargs):
        self._authentication = kwargs.pop('authentication', None)
        super(CustomClient, self).__init__(*args, **kwargs)

    def open(self, *args, **kwargs):
        if self._authentication:
            kwargs.setdefault('headers', {})
            headers = kwargs['headers']
            headers['Authorization'] = self._authentication
        if 'json' in kwargs:
            kwargs['data'] = json.dumps(kwargs.pop('json'))
            kwargs['content_type'] = 'application/json'
        return super(CustomClient, self).open(*args, **kwargs)


class FlaskTestCase(flask_testing.TestCase):

    def create_app(self):
        app.config['TESTING'] = True
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
        app.test_client_class = CustomClient
        return app

    def setUp(self):
        super(FlaskTestCase, self).setUp()
        db.drop_all()
        db.create_all()
        self.depot = manager.DepotManager.get()

    def tearDown(self):
        super(FlaskTestCase, self).tearDown()
        db.session.remove()

    def get_or_create_admin(self):
        user = models.User.query.filter_by(username='admin').first()
        if not user:
            user = models.User(username='admin', password='123')
            user.save()
        return user

    @property
    def admin(self):
        return self.get_or_create_admin()

    @property
    def admin_client(self):
        return self.app.test_client(
            authentication=b'Bearer ' + self.admin.generate_auth_token())
