import click
import flask
import flask_security
from flask_admin import Admin
from flask_admin import helpers as admin_helpers
from flask_admin.contrib.sqla import ModelView
from flask_security import cli as security_cli
from wtforms import validators
from wtforms.fields import PasswordField

from project.instances import app
from project.instances import db
from project import models


# ############################# SCURITY ###############################


# Setup Flask-Security
user_datastore = flask_security.SQLAlchemyUserDatastore(
    db, models.User, models.Role)
security = flask_security.Security(app, user_datastore)


@security.context_processor
def security_context_processor():
    return dict(
        admin_base_template=admin.base_template,
        admin_view=admin.index_view,
        h=admin_helpers,
        get_url=flask.url_for
    )

# ############################# ADMIN #################################


admin = Admin(app, 'My Project',
              base_template='my_master.html',
              template_mode='bootstrap3')


def admin_view(cls):
    admin.add_view(cls(cls.model_cls, db.session))
    return cls


# Create customized model view class
class BaseModelView(ModelView):
    model_cls = None
    required_role = None
    form_excluded_columns = ['created', 'updated']

    def is_accessible(self):
        cur_user = flask_security.current_user
        if not cur_user.is_active or not cur_user.is_authenticated:
            return False

        if self.required_role:
            if not cur_user.has_role(self.required_role):
                return False

        return True

    def _handle_view(self, name, **kwargs):
        """ Override builtin _handle_view in order to redirect
            users when a view is not accessible.
        """
        if not self.is_accessible():
            if flask_security.current_user.is_authenticated:
                # permission denied
                flask.abort(403)
            else:
                # login
                return flask.redirect(flask.url_for(
                    'security.login', next=flask.request.url))


@admin_view
class UserView(BaseModelView):
    model_cls = models.User
    required_role = 'admin'

    column_exclude_list = (
        'password',
        'password_hash',
    )
    form_extra_fields = {
        'password': PasswordField('Password', [validators.required()])
    }
    form_columns = (
        'email',
        'password',
    )


@admin_view
class RoleView(BaseModelView):
    model_cls = models.Role
    required_role = 'admin'

    form_columns = (
        'name',
        'description',
    )


@click.group()
def cli():
    pass


cli.add_command(security_cli.users)
cli.add_command(security_cli.roles)


if __name__ == '__main__':
    cli()
