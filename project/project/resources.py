from flask_restful import reqparse
import flask
import flask_restful

import project
from project import config
from project import models
from project.instances import api
from project.instances import basic_auth
from project.instances import token_auth


def _405():
    flask_restful.abort(
        405, message='The method is not allowed for the requested URL.')


class BaseListResource(flask_restful.Resource):

    MODEL = None
    POST_ENABLED = True
    PARSER = reqparse.RequestParser()

    def get(self):
        args = self.PARSER.parse_args(strict=True)
        return [obj.to_dict()
                for obj in self.MODEL.query.filter_by(**args).all()]

    @token_auth.login_required
    def post(self):
        if not self.POST_ENABLED:
            _405()
        args = self.PARSER.parse_args(strict=True)
        obj = self.MODEL.from_dict(args)
        obj.save()
        return obj.to_dict(), 201


class BaseResource(flask_restful.Resource):

    MODEL = None
    PUT_ENABLED = True
    DELETE_ENABLED = True

    def get(self, obj_id):
        return self.MODEL.query.get_or_404(obj_id).to_dict()

    @token_auth.login_required
    def put(self, obj_id):
        if not self.PUT_ENABLED:
            _405()
        obj = self.MODEL.query.get_or_404(obj_id)
        args = self.PARSER.parse_args(strict=True)
        obj.update_from_dict(args)
        obj.save()
        return obj.to_dict(), 200

    @token_auth.login_required
    def delete(self, obj_id):
        if not self.DELETE_ENABLED:
            _405()
        obj = self.MODEL.query.get_or_404(obj_id)
        self.on_delete(obj)
        obj.delete()
        return '', 204

    def on_delete(self, obj):
        pass


########################################################################
#                               API                                    #
########################################################################


@basic_auth.verify_password
def verify_password(username, password):
    flask.g.user = None
    user = models.User.query.filter_by(email=username).first()
    if not user or not user.verify_password(password):
        return False
    flask.g.user = user
    return True


@token_auth.verify_token
def verify_token(token):
    flask.g.user = None
    user = models.User.verify_auth_token(token)
    if not user:
        return False
    flask.g.user = user
    return True


@api.resource('/api/')
class ApiResource(flask_restful.Resource):

    def get(self):
        return {'version': project.__version__}


@api.resource('/api/token/')
class ApiTokenResource(flask_restful.Resource):

    @basic_auth.login_required
    def get(self):
        token = flask.g.user.generate_auth_token()
        return {'token': token.decode('ascii'),
                'duration': config.TOKEN_EXPIRES}


@api.resource('/api/check_token/')
class ApiTokenTestResource(flask_restful.Resource):

    @token_auth.login_required
    def get(self):
        return {'message': 'token is valid'}


########################################################################
#                                                                      #
########################################################################


# @api.resource('/api/my/')  # TODO: rename
# class NodesResource(BaseListResource):
#     MODEL = models.MyModel
#     PARSER = models.MyModel.get_fields_parser()
#     POST_ENABLED = False


# @api.resource('/api/my/<int:obj_id>/')  # TODO: rename
# class NodeResource(BaseResource):
#     MODEL = models.MyModel
#     PARSER = models.MyModel.get_fields_parser()
#     PUT_ENABLED = False
#     DELETE_ENABLED = False
